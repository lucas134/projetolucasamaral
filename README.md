# projetoLucasAmaral

Pacote de exemplo do Lucas Amaral

## Instalação

> Requer PHP 7 e a extensão `mbstring`

```bash
composer require user/projetolucasamaral
```

## API

namespace user\projetolucasamaral;
class Exemplo{
    function nome(){
        return "";
    }
}

## Licença
MIT